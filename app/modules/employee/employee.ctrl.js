'use strict';

(function (angular) {
    'use strict';

    angular.module('app').controller('employeeCtrl', employeeCtrl);

    employeeCtrl.$inject = ['$scope', '$rootScope', 'commonSrv', 'constantSrv'];

    function employeeCtrl($scope, $rootScope, commonSrv, constantSrv) {
        var vm = this;
        vm.loading = true;
        vm.employees = [];
        vm.perEmployee = [];
        vm.monthlyData = [];
        vm.totalLeaves = [];
        vm.calLeaves = [];
        vm.selectedEmployee = {};
        vm.employRecord = employRecord;
        vm.tabName = 'view';
        var todayDate = vm.todayDate = new Date();
        vm.todayDate = moment(todayDate).format("DD-MM-YYYY");
        vm.empStatus = 'Absent';

        vm.init = function() {
            getEmployee();
            getHolidays();
        }

        function getEmployee() {
            vm.employees = localStorage.getItem('employs') ? JSON.parse(localStorage.getItem('employs')) : [];
            vm.employees = _.orderBy(vm.employees, ['type','Name'], ['asc']);
            commonSrv.getEmployee('getemployee').then(function(response) {
                if(_.get(_.get(response, 'result'), 'length')){
                    vm.employees = response.result;
                    vm.employees = _.orderBy(vm.employees, ['type','Name'], ['asc']);
                    localStorage.setItem('employs', JSON.stringify(vm.employees));
                    employRecord(vm.employees[0]);
                    vm.selectedEmployee = vm.employees[0];
                } else {
                    vm.employees = vm.employees || [];
                    vm.selectedEmployee = {};
                    vm.perEmployee = [];
                    vm.monthlyData = [];
                    vm.totalLeaves = [];
                    vm.calLeaves = [];
                    vm.empStatus = 'Absent';
                    vm.absent = undefined;
                    if(vm.employees.length > 0) {
                        employRecord(vm.employees[0]);
                    }
                }
            })
        }

        vm.changeTab = function(tab){
            vm.tabName = tab
        }

        function employRecord(record) {
            var params = {
                "EnrollNumber":record.EnrollNumber,
                "DateFrom":"",
                "DateTo":"",
                "IsAbsent":false
            }
            vm.isRecent = true;
            vm.tabName = 'view';
            commonSrv.getRecords(params, 'gettimesheet').then(function(response) {
                vm.isRecent = false;
                if(_.get(_.get(response, 'result'), 'length')) {
                    vm.perEmployee = response.result;
                    if(vm.perEmployee[0].InOutMode == '1' || vm.perEmployee[0].InOutMode == '101') {
                        vm.empStatus = 'Present';
                    }
                } else {
                    vm.perEmployee = [];
                    vm.empStatus = 'Absent';
                }
            });
            monthRecord(record);
            getLeaves(record);
            vm.selectedEmployee = record;
            //calculateLeaves(record);
        }

        function monthRecord(record) {
            var dayData = constantSrv.yearList[todayDate.getMonth()];
            var params = {
                "EnrollNumber":record.EnrollNumber,
                "DateFrom":todayDate.getFullYear()+"-"+dayData.month+"-01",
                "DateTo":todayDate.getFullYear()+"-"+dayData.month+"-"+dayData.endDay,
                "IsAbsent":false
            }
            commonSrv.getRecords(params, 'gettimesheet').then(function(response) {
                if(_.get(_.get(response, 'result'), 'length')){
                    vm.monthlyData = response.result;
                } else {
                    vm.monthlyData = [];
                }
            })
        }

        function getLeaves(record) {
            var dayData = constantSrv.yearList[todayDate.getMonth()];
            var params = {
                "EnrollNumber":record.EnrollNumber
            }
            vm.isAbsent = true;
            commonSrv.getRecords(params, 'getLeaves').then(function(response) {
                vm.isAbsent = false;
                if(_.get(_.get(response, 'result'), 'length')) {
                    vm.totalLeaves = response.result;
                    vm.absent = vm.totalLeaves.length;
                } else {
                    vm.absent = 0;
                    vm.totalLeaves = [];
                }
            })
        }

        function calculateLeaves(record) { 
            var dayData = constantSrv.yearList[todayDate.getMonth()];
            var params = {
                "EnrollNumber":record.EnrollNumber
            }
            vm.isAbsent = true;
            commonSrv.getRecords(params, 'calculateLeaves').then(function(response) {
                vm.isAbsent = false;
                if(_.get(_.get(response, 'result'), 'length')) {
                    vm.calLeaves = response.result;
                } else {
                    vm.calLeaves = [];
                }
                getLeaves(record);
            })
        }

        vm.refresh = function() {
            if(_.get(vm.selectedEmployee, 'Name')){
                calculateLeaves(vm.selectedEmployee);
            }
        }

        vm.searchData = function(data) {
            var start = new Date(data.startDate);
            var end = new Date(data.endDate);
            var sMonth = (((start.getMonth()+1).toString()).length > 1 ? (start.getMonth()+1) : '0' + (start.getMonth()+1));
            var eMonth = (((end.getMonth()+1).toString()).length > 1 ? (end.getMonth()+1) : '0' + (end.getMonth()+1));
            var sDay = ((start.getDate().toString()).length > 1 ? start.getDate() : '0' + start.getDate());
            var eDay = (end.getDate().toString().length > 1 ? end.getDate() : '0' + end.getDate());
            var params = {
                "EnrollNumber":vm.selectedEmployee.EnrollNumber,
                "DateFrom": start.getFullYear()+"-"+sMonth+"-"+sDay,
                "DateTo": end.getFullYear()+"-"+eMonth+"-"+eDay,
                "IsAbsent":false
            }
            vm.isFilter = true;
            commonSrv.getRecords(params, 'gettimesheet').then(function(response) {
                vm.isFilter = false;
                if(_.get(_.get(response, 'result'), 'length')) {
                    vm.filterData = response.result;
                    _.forEach(vm.filterData, function(value) {
                        value.inOrOut =  value.InOutMode == '1'? 'In' : 'Out';
                        value.date =  moment(new Date(value.DateTimeRecord)).format('DD/MM/YYYY');
                        value.time =  moment(new Date(value.DateTimeRecord), "HH:mm:ss").format("hh:mm A");
                    })
                } else {
                    vm.filterData = [];
                }
            })
        }

        function getHolidays() {
            commonSrv.getHolidays('getHolidays').then(function(response) {
                if(_.get(_.get(response, 'result'), 'length')){
                    vm.holidayList = response.result;
                } else {
                    vm.holidayList =  [];
                }
            })
        };

        vm.syncData = function() {
            vm.isRecent = true;
            commonSrv.syncData().then(function(response) {
                employRecord(vm.selectedEmployee);
            })
        }
    }
})(window.angular);