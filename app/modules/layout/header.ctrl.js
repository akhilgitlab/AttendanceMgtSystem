'use strict';

(function (angular) {
    'use strict';

    angular.module('app').controller('headerCtrl', headerCtrl);

    headerCtrl.$inject = ['$scope', '$rootScope', 'commonSrv', '$state'];

    function headerCtrl($scope, $rootScope, commonSrv, $state) {
        var vm = this;
        vm.init = function(){

        }

        vm.changed = function(){
            localStorage.setItem('userId', '');
            localStorage.setItem('employs', '');
            $state.go('/login');
        }

    }
})(window.angular);