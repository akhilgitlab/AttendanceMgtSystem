'use strict';

(function (angular) {
    'use strict';

    angular.module('app').controller('loginAuthCtrl', loginAuthCtrl);

    loginAuthCtrl.$inject = ['$rootScope', '$state', 'commonSrv'];

    function loginAuthCtrl($rootScope, $state, commonSrv) {

        var vm = this;
        vm.loading = false;

        vm.login = {
            userName: '',
            password: ''
        };

        vm.doLogin = function (data) {
            if(data) {
                vm.loading = true;
                commonSrv.auth({username: data.userName, password: data.password}, 'login').then(function(response){
                    vm.loading = false;
                    if(_.get(response, 'result')) {
                        localStorage.setItem('userId', response.result[0].docId);
                        localStorage.setItem('userName', response.result[0].username);
                        $state.go('app.admin.employee');
                    } else{
                        //toastSrv.showError('Something went wrong');
                    }
                })
            }
        };
    };
})(window.angular);