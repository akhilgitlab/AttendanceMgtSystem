'use strict';

angular.module('app').directive('uiLoader', function () {
  return {
    restrict: 'EA',
    scope: {
      loading: '=loading'
    },
    template: '<div id="loaderDivPage" class="load-main" ng-show="loading">\n<div class="spinner">\n<div class="rect1"></div>\n<div class="rect2"></div>\n<div class="rect3"></div>\n<div class="rect4"></div>\n<div class="rect5"></div>\n</div>\n</div>',
    link: function link(scope, element, attrs) {}
  };
});