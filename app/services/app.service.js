'use strict';

(function (app) {
    'use strict';

    angular.module('app').factory('commonSrv', commonSrv);

    commonSrv.$inject = ['$timeout', '$window', '$rootScope', '$http'];

    function commonSrv($timeout, $window, $rootScope, $http) {

        var vm = this;
        var baseUrl = "https://us-central1-new-bio.cloudfunctions.net/";
        var sringUrl = "http://b4eec1d7.ngrok.io/new-bio/us-central1/syncData";

        var service = {
            item: {},
            auth,
            getEmployee,
            getRecords,
            syncData,
            getHolidays
        };

        function auth(params, module) {
            return $http({
                method : "POST",
                url : baseUrl+module,
                data: params
            }).then(function mySuccess(response) {
                if(_.get(response.data, 'message') == 'ok'){
                    return response.data
                } else{
                    return false;
                }
            }, function myError(response) {
                return false;
            });
        }

        function getEmployee(module) {
            return $http({
                method : "POST",
                url : baseUrl+module
            }).then(function mySuccess(response) {
                return response.data;
            }, function myError(response) {
                return response.data;
            });
        }

        function getRecords(params, module) {
            return $http({
                method : "POST",
                url : baseUrl+module,
                data: params
            }).then(function mySuccess(response) {
                if(_.get(response.data, 'message') == 'ok'){
                    return response.data
                } else{
                    return false;
                }
            }, function myError(response) {
                return false;
            });
        }

        function syncData(params, module) {
            return $http({
                method : "POST",
                url : sringUrl,
            }).then(function mySuccess(response) {
                if(_.get(response.data, 'message') == 'ok'){
                    return response.data
                } else{
                    return false;
                }
            }, function myError(response) {
                return false;
            });
        }

        function getHolidays(module){
            return $http({
                method : "POST",
                url : baseUrl+module
            }).then(function mySuccess(response) {
                if(_.get(response.data, 'message') == 'ok'){
                    return response.data
                } else{
                    return false;
                }
            }, function myError(response) {
                return false;
            });
        };

        return service;
    }
})(window.app);