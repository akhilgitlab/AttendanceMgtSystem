'use strict';

(function (app) {
    'use strict';

    angular.module('app').constant('constantSrv', {
        yearList: {
            0: {mth: 'January', month: '01', endDay: '31'},
            1: {mth: 'February', month: '02', endDay: (new Date()).getFullYear % 4 == 0 ? 29 : 28},
            2: {mth: 'March', month: '03', endDay: '31'},
            3: {mth: 'April', month: '04', endDay: '30'},
            4: {mth: 'May', month: '05', endDay: '31'},
            5: {mth: 'June', month: '06', endDay: '30'},
            6: {mth: 'July', month: '07', endDay: '31'},
            7: {mth: 'August', month: '08', endDay: '31'},
            8: {mth: 'September', month: '09', endDay: '30'},
            9: {mth: 'October', month: '10', endDay: '31'},
            10: {mth: 'November', month: '11', endDay: '30'},
            11: {mth: 'December', month: '12', endDay: '31'}
        }
    });
})(window.app);