'use strict';

(function (app) {
    'use strict';

    angular.module('app').factory('toastSrv', toastSrv);
    
    toastSrv.$inject = ['$mdToast'];

    function toastSrv($mdToast) {

        this.errorMessage = null;
        this.successMessage = null;

        var service = {
            message: '',
            showError: showError,
            showSuccess: showSuccess,
            showCustomError: showCustomError,
            showCustomSuccess: showCustomSuccess,
            dismissToast: dismissToast
        };
        return service;

        function showError(errorMessage) {
            return $mdToast.show($mdToast.simple().textContent(errorMessage).position('top right').hideDelay(1000000).theme('error-toast').action('OK').highlightAction(false));
        };

        function showSuccess(successMessage) {
            return $mdToast.show($mdToast.simple().textContent(successMessage).position('top right').hideDelay(2000).theme('success-toast')
            //.action('OK')
            .highlightAction(false));
        };

        function showCustomError(templateCtrl, templateUrl, position, hideDelay) {
            //this.errorMessage = errorMessage;
            // controller pattern wiht template

            var templateCtrl = templateCtrl ? templateCtrl : 'toastErrCtrl';
            var position = position ? position : 'top left';
            var templateUrl = templateUrl ? templateUrl : 'app/shared/toast/toastErrTemplate.html';
            var hideDelay = hideDelay ? hideDelay : 10000;

            return $mdToast.show({
                controller: templateCtrl,
                templateUrl: templateUrl,
                hideDelay: hideDelay,
                position: position
            });
        };

        function showCustomSuccess(templateCtrl, templateUrl, position, hideDelay) {
            //this.successMessage = successMessage;
            // controller pattern wiht template

            var templateCtrl = templateCtrl ? templateCtrl : 'toastErrCtrl';
            var position = position ? position : 'top left';
            var templateUrl = templateUrl ? templateUrl : 'app/shared/toast/toastSuccecsTemplate.html';
            var hideDelay = hideDelay ? hideDelay : 2000;

            return $mdToast.show({
                controller: templateCtrl,
                templateUrl: templateUrl,
                hideDelay: hideDelay,
                position: position
            });
            var obj = {};
            obj.id = 2;
            message.push(obj);
            showTost(message);
        };

        function showTost(message) {
            //this.successMessage = successMessage;
            // controller pattern wiht template
            var templateCtrl = templateCtrl ? templateCtrl : 'toastErrCtrl';
            var position = position ? position : 'top left';
            var templateUrl = templateUrl ? templateUrl : 'app/shared/toast/toastSuccecsTemplate.html';
            var hideDelay = hideDelay ? hideDelay : 2000;

            return $mdToast.show({
                controller: templateCtrl,
                templateUrl: templateUrl,
                hideDelay: hideDelay,
                position: position
            });

            if (message.length > 0) {
                var obj = message[0];
                $mdToast.show(obj).then(function () {
                    obj.index;
                    remove(message(obj.index));
                    showTost(message);
                });
            };
        };

        function dismissToast() {
            $mdToast.hide();
        };
    }
})(window.app);