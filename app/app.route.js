'use strict';

(function (app) {
    'use strict';

    var configure = ['$stateProvider', '$urlRouterProvider', '$locationProvider', function ($stateProvider, $urlRouterProvider, $locationProvider) {

        $stateProvider.state('app', {
            cache: true,
            views: {
                '': {
                    templateUrl: 'app/modules/landing/landing.html'
                },
                'header@app': {
                    templateUrl: 'app/modules/layout/header.html',
                    controller: 'headerCtrl as $hdr'
                },
                'footer@app': {
                    templateUrl: 'app/modules/layout/footer.html'
                }
            }
        }).state('app.admin', {
            display_name: 'Admin',
            url: '/admin',
            cache: true,
            views: {
                'pageContent': {
                    templateUrl: 'app/modules/admin/admin.html',
                    controller: 'adminCtrl as $adm'
                }
            }
        }).state('app.admin.employee', {
            display_name: 'Employee',
            url: '/employee',
            cache: true,
            views: {
                'rightContent': {
                    templateUrl: 'app/modules/employee/employee.html',
                    controller: 'employeeCtrl as $empy'
                }
            }
        })

        .state('/login', {
            url: '/login',
            cache: true,
            templateUrl: 'app/controls/auth/login.html',
            controller: 'loginAuthCtrl as $log'
        })
        //$locationProvider.html5Mode(true);
    }];

    var run = ['$rootScope', '$state', function ($rootScope, $state) {

        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            var isUser = localStorage.getItem('userId');
            if(!isUser && toState.name != "/login") {
                $state.go('/login');
                event.preventDefault();
            } else if(isUser && toState.name == "/login") {
                $state.go('app.admin.employee');
                event.preventDefault();
            }
        });
    }];

    angular.module('app').run(run);

    angular.module('app').config(configure);
})(window.app);
