(function(){
    'use strict';
    angular.module('app', ['ui.router', 'ui.bootstrap'])
        .config(configure)
        .run(runBlock);

        configure.$inject = ['$urlRouterProvider', '$provide', '$httpProvider'];
        function configure($urlRouterProvider, $provide, $httpProvider) {
            $urlRouterProvider.otherwise('/login');
        }

        runBlock.$inject = ['$rootScope','$state']; 
        function runBlock($rootScope, $state) {
            
        }
})();